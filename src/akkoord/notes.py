from enum import Enum
from typing import Any, Self

CHROMATIC_SCALE = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]


class Interval(Enum):
    Root = 0
    PerfectUnison = 0
    DiminishedSecond = 0
    MinorSecond = 1
    AugmentedUnison = 1
    MajorSecond = 2
    DiminishedThird = 2
    MinorThird = 3
    AugmentedSecond = 3
    MajorThird = 4
    DiminishedFourth = 4
    PerfectFourth = 5
    AugmentedThird = 5
    DiminishedFifth = 6
    AugmentedFourth = 6
    PerfectFifth = 7
    DiminshedSixth = 7
    MinorSixth = 8
    AugmentedFifth = 8
    MajorSixth = 9
    DiminishedSeventh = 9
    MinorSeventh = 10
    AugmentedSixth = 10
    MajorSeventh = 11
    DiminishedOctave = 11
    PerfectOctave = 12
    AugmentedSeventh = 12


class Note:
    def __init__(self, note: str) -> None:
        self.note = note.capitalize()

        if len(note) > 1 and note[-1] not in ("#", "b"):
            raise ValueError(f"'{self.note}'")

    @property
    def sharp(self) -> bool:
        return self.note[-1] == "#"

    @property
    def flat(self) -> bool:
        return self.note[-1] == "b"

    # TODO: return a new note instead of 'Self'
    def lower(self, semitones: int = 1) -> Self:
        idx = CHROMATIC_SCALE.index(self.note)
        note = CHROMATIC_SCALE[(idx - semitones) % len(CHROMATIC_SCALE)]
        return self.__class__(note)

    def raise_(self, semitones: int = 1) -> Self:
        idx = CHROMATIC_SCALE.index(self.note)
        note = CHROMATIC_SCALE[(idx + semitones) % len(CHROMATIC_SCALE)]
        return self.__class__(note)

    def enharmonic(self) -> Self:
        if any([self.sharp, self.flat]):
            idx = CHROMATIC_SCALE.index(self.note[0])
            if self.sharp:
                note = (
                    CHROMATIC_SCALE[(idx + 2) % len(CHROMATIC_SCALE)].strip("b#") + "b"
                )
                return self.__class__(note)
            elif self.flat:
                note = (
                    CHROMATIC_SCALE[(idx - 2) % len(CHROMATIC_SCALE)].strip("b#") + "#"
                )
                return self.__class__(note)
        return self.__class__(self.note)

    def __str__(self) -> str:
        return self.note

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.note})"

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, Note):
            return False
        return self.note == other.note
