import re
from typing import Iterator, Sequence

import more_itertools as mi

from akkoord.notes import Note
from akkoord.scales import ChromaticScale


class String:
    def __init__(self, tonic: str) -> None:
        self.tonic = tonic
        self.scale = ChromaticScale(tonic)
        self.notes = self.scale.notes

    def frets(self, note: str) -> list[int]:
        return list(mi.locate(self.notes, lambda x: x.note == Note(note).note))

    def note(self, fret: int) -> Note | None:
        try:
            return self.notes[fret]
        except IndexError:
            return None

    def segments(self) -> Iterator["StringSegment"]:
        for grp in mi.windowed(range(len(self.notes)), 4):
            yield StringSegment(str(self.scale.tonic), grp)

    def segment(self, frets: list[int]) -> "StringSegment":
        return StringSegment(str(self.scale.tonic), frets)

    def __repr__(self):
        return f"{self.__class__.__name__}(tonic={self.scale.tonic})"

    def __str__(self) -> str:
        return f"{self.scale.tonic}"


class StringSegment(String):
    """Segment of 4 frets."""

    def __init__(self, tonic: str, target_frets: Sequence[int | None]):
        self.scale = ChromaticScale(tonic)
        self.target_frets = target_frets
        self.notes = []
        for n in self.target_frets:
            if n is not None:
                self.notes.append(self.scale.notes[n])


class Tuning:
    tuning: str
    strings: list[String]

    def __init__(self, tuning: str = "EADGBE") -> None:
        self.tuning = tuning
        self.strings = [String(t.group(0)) for t in re.finditer(r"\w(#|b)?", tuning)]

    def string(self, note):
        for s in self.strings:
            if s.tonic == note:
                return s


if __name__ == "__main__":
    print(Tuning("EADGBE").strings)

    print(String("A").notes)
    print(String("A").segment([1, 2, 3, 4]).notes)
