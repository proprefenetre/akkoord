#! /usr/bin/env python
from typing import Type

import click

from akkoord.chord import (
    AugmentedChord,
    Chord,
    DiminishedChord,
    MajorChord,
    MajorSeventhChord,
    MinorChord,
    MinorSeventhChord,
    SuspendedFourthChord,
    SuspendedSecondChord,
)
from akkoord.scales import ChromaticScale, MajorScale, MinorScale
from akkoord.tuning import Tuning

ChordT = Type[
    MajorChord
    | MinorChord
    | MajorSeventhChord
    | MinorSeventhChord
    | DiminishedChord
    | AugmentedChord
    | SuspendedSecondChord
    | SuspendedFourthChord
]


@click.command()
@click.option("--string", "-s")
@click.option("--chord", "-c")
@click.option("--tuning", "-t", default="EADGBE")
@click.option("--scale", "-S")
@click.option("--degrees", "-d")
def run(string: str, chord: str, tuning: str, scale: str, degrees: str):
    tuning_: Tuning = Tuning(tuning)

    if string:
        frets = "| " + " | ".join(f"{n: >2}" for n in range(1, 13)) + " |"
        click.echo(frets)
        click.echo("-" * len(frets))
        for s in string:
            notes = [*list(tuning_.string(s).notes[1:]), s]
            click.echo("| " + " | ".join(f"{n!s: <2}" for n in notes) + " |")

    if chord:
        chord_type: ChordT
        if chord.endswith("m"):
            chord_type = MinorChord
        elif chord.endswith("m7"):
            chord_type = MinorSeventhChord
        elif chord.endswith("7"):
            chord_type = MajorSeventhChord
        else:
            chord_type = MajorChord

        click.echo(
            f"{chord_type(chord).name.capitalize()}: "
            + ", ".join(str(n) for n in chord_type(chord).notes)
        )

    if scale:
        if len(scale) == 1:
            click.echo(", ".join(str(n) for n in ChromaticScale(scale).notes))
        elif "m" in scale:
            click.echo(", ".join(str(n) for n in MinorScale(scale).notes))
        else:
            click.echo(", ".join(str(n) for n in MajorScale(scale).notes))

    if degrees:
        scale_: MinorScale | MajorScale
        if degrees.endswith("m"):
            scale_ = MinorScale(degrees.removesuffix("m"))
        else:
            scale_ = MajorScale(degrees)
        click.echo(f"{scale_}: {', '.join(n.note for n in scale_.notes)}")
        chords: list[Chord] = []
        for triad, note in zip(scale_.triads, scale_.notes):
            if "o" in triad:
                chords.append(DiminishedChord(note.note))
            elif triad.islower():
                chords.append(MinorChord(note.note))
            elif triad.isupper():
                chords.append(MajorChord(note.note))
        for deg, ch in zip(scale_.triads, chords):
            click.echo(f"{deg: <5} {ch.name: <5} {ch}")


if __name__ == "__main__":
    # run()
    print(MinorScale("D"))
    print(MinorScale("D").enharmonic())

    print(ChromaticScale("C#"))
    print(ChromaticScale("C#").enharmonic())

    print(ChromaticScale("C"))
    print(ChromaticScale("C").enharmonic())

    # chord diagram
    # def diagram(chord: MajorChord | MinorChord):
    #     neck = Tuning("EADGBe")
    #     chord_notes = [n.note for n in chord.notes]

    #     print(chord.name)
    #     frets = "".join(f"|{n: ^8}" for n in (4, 3, 2, 1))
    #     print(frets)
    #     print("=" * len(frets) + "==")
    #     for s in reversed(neck.strings):
    #         notes = list(reversed([n.note for n in s.segment([1, 2, 3, 4]).notes]))
    #         for cn in chord_notes:
    #             if cn in notes:
    #                 notes[notes.index(cn)] = f"[{cn}]"
    #                 chord_notes.remove(cn)
    #         print("".join(f"|{n: ^8}" for n in notes) + f"||{s.tonic: ^8}")
    #     print()

    # diagram(MajorChord("D"))
    # diagram(MajorChord("A"))
    # diagram(MinorChord("A"))

    # # vertical diagrams
    # neck = Tuning()
    # strings = [s.segment([1, 2, 3, 4]).notes for s in neck.strings]
    # frets = []
    # for n in range(4):
    #     frets.append([s[n] for s in reversed(strings)])

    # open_notes = "".join(f"|{n: ^4}" for n in ("eBGDAE")) + "|"
    # print(open_notes)
    # print("+" + (len(open_notes)-2) * "=" + "+")
    # for f in frets:
    #     print("".join(f"|{n.note: ^4}" for n in f) + "|")
