from akkoord.notes import Interval, Note
from akkoord.scales import ChromaticScale


class Chord:
    def __init__(self, name: str):
        self.name: str = name
        self.scale: ChromaticScale = ChromaticScale(name)
        self.intervals: list[Interval] = []

    @property
    def notes(self) -> list[Note]:
        return [self.scale[i.value] for i in self.intervals]

    def __str__(self) -> str:
        return "-".join(f"{n}" for n in self.notes)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.name})"


class MajorChord(Chord):
    "Root, major third, perfect fifth"
    suffix = ""

    def __init__(self, name: str):
        self.name = name
        self.scale = ChromaticScale(name)
        self.intervals = [Interval.Root, Interval.MajorThird, Interval.PerfectFifth]


class MinorChord(Chord):
    "Root, minor third, perfect fifth"
    suffix = "m"

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name.removesuffix(self.suffix))
        self.intervals = [Interval.Root, Interval.MinorThird, Interval.PerfectFifth]


class MajorSeventhChord(Chord):
    "Root, major third, perfect fifth, major seventh"
    suffix = "maj7"

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name.removesuffix("maj7"))
        self.intervals = [
            Interval.Root,
            Interval.MajorThird,
            Interval.PerfectFifth,
            Interval.MajorSeventh,
        ]


class MinorSeventhChord(Chord):
    "Root, minor third, perfect fifth, minor seventh"
    suffix = "m7"

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name.removesuffix("m7"))
        self.intervals = [
            Interval.Root,
            Interval.MinorThird,
            Interval.PerfectFifth,
            Interval.MinorSeventh,
        ]


class AugmentedChord(Chord):
    "Root, major third, augmented fifth"
    suffix = "aug"

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name.removesuffix("aug"))
        self.intervals = [Interval.Root, Interval.MajorThird, Interval.AugmentedFifth]


class DiminishedChord(Chord):
    "Root, minor third, diminished fifth"
    suffix = "dim"

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name.removesuffix("dim"))
        self.intervals = [Interval.Root, Interval.MinorThird, Interval.DiminishedFifth]


class SuspendedSecondChord(Chord):
    "Root, perfect fifth, perfect fourth"
    suffix = "sus2"

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name.removesuffix("sus2"))
        self.intervals = [Interval.Root, Interval.MajorSecond, Interval.PerfectFifth]


class SuspendedFourthChord(Chord):
    "Root, perfect fifth, perfect fourth"
    suffix = "sus4"

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name.removesuffix("sus4"))
        self.intervals = [Interval.Root, Interval.PerfectFourth, Interval.PerfectFifth]


class DominantSeventhChord(Chord):
    "Root, major third, perfect fifth, minor seventh"
    suffix = "7"

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name.removesuffix("7"))
        self.intervals = [
            Interval.Root,
            Interval.MajorThird,
            Interval.PerfectFifth,
            Interval.MinorSeventh,
        ]


class PowerChord(Chord):
    suffix = ""

    def __init__(self, name: str):
        self.name = f"{name}{self.suffix}" if not name.endswith(self.suffix) else name
        self.scale = ChromaticScale(name)
        self.intervals = [Interval.Root, Interval.PerfectFifth]


if __name__ == "__main__":
    print(MajorChord("C"))
    print()

    cmaj7 = MajorSeventhChord("Cmaj7")
    print("Cmaj7:", cmaj7)
    print()

    dbmaj7 = MajorSeventhChord("Dbmaj7")
    print("Dbmaj7:", cmaj7)
    print()

    cm7 = MinorSeventhChord("Cm7")
    print("Cm7:", cm7)
    print()

    caug = AugmentedChord("Caug")
    print("caug:", caug)
    print()

    cdim = DiminishedChord("Cdim")
    print("cdim:", cdim)
    print("cdim:", ", ".join(n.enharmonic().note for n in cdim.notes))
    print()

    Dbdim = DiminishedChord("Dbdim")
    print("Dbdim:", Dbdim)
    print()

    C7 = DominantSeventhChord("C7")
    print("C7:", C7)
    print()

    Csus4 = SuspendedFourthChord("C")
    print("Csus4:", Csus4)
    print()

    Csus2 = SuspendedSecondChord("C")
    print("Csus2:", Csus2)
    print()

    D = PowerChord("D")
    print("D:", D)
