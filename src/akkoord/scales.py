from collections import deque
from typing import Any, Self

from akkoord.notes import CHROMATIC_SCALE, Interval, Note


class ChromaticScale:
    chromatic_scale: deque[Note]

    def __init__(self, tonic: str | Note = "C") -> None:
        self.chromatic_scale = deque(Note(n) for n in CHROMATIC_SCALE)

        self.tonic = Note(tonic.note if isinstance(tonic, Note) else tonic)

        if self.tonic not in self.chromatic_scale:
            self.chromatic_scale = deque(n.enharmonic() for n in self.chromatic_scale)

        self.chromatic_scale.rotate(-self.chromatic_scale.index(self.tonic))

        self.notes = list(self.chromatic_scale)

    def enharmonic(self) -> Self:
        enh = self.__class__(self.tonic.enharmonic().note)
        enh.chromatic_scale = deque(n.enharmonic() for n in self.chromatic_scale)
        enh.chromatic_scale.rotate(-enh.chromatic_scale.index(enh.tonic))
        enh.notes = [n.enharmonic() for n in self.notes]
        return enh

    def index(self, note: str) -> int:
        return self.notes.index(Note(note))

    def __getitem__(self, interval: int) -> Note:
        return self.notes[interval % len(self.notes)]

    def __str__(self) -> str:
        return ", ".join(n.note for n in self.notes)

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, ChromaticScale):
            return False
        return self.chromatic_scale == other.chromatic_scale


class DiatonicScale(ChromaticScale):
    intervals: tuple[Interval, ...]
    formula: tuple[int, ...]
    triads: tuple[str, ...]

    def __init__(self, tonic: str | Note = "C"):
        super().__init__(tonic.note if isinstance(tonic, Note) else tonic)
        self.notes = []
        for interval in self.intervals:
            self.notes.append(self.chromatic_scale[interval.value])

    def __str__(self) -> str:
        return (
            f"{self.tonic} {self.__class__.__name__.removesuffix('Scale')}:"
            f"{', '.join(n.note for n in self.notes)}"
        )


class MajorScale(DiatonicScale):
    intervals = (
        Interval.Root,
        Interval.MajorSecond,
        Interval.MajorThird,
        Interval.PerfectFourth,
        Interval.PerfectFifth,
        Interval.MajorSixth,
        Interval.MajorSeventh,
    )
    formula = (2, 2, 1, 2, 2, 2, 1)
    triads = ("I", "ii", "iii", "IV", "V", "vi", "viio")


class MinorScale(DiatonicScale):
    """Natural minor scale."""

    intervals = (
        Interval.Root,
        Interval.MajorSecond,
        Interval.MinorThird,
        Interval.PerfectFourth,
        Interval.PerfectFifth,
        Interval.MinorSixth,
        Interval.MinorSeventh,
    )
    formula = (2, 1, 2, 2, 1, 2, 2)
    triads = ("i", "iio", "III", "iv", "v", "VI", "VII")
