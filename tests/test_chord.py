from akkoord import chord
from akkoord.notes import Note
from akkoord.scales import ChromaticScale


def test_chord_initialization():
    assert chord.MinorChord("C").name == "Cm"
    assert chord.MajorSeventhChord("Db").name == "Dbmaj7"
    assert chord.MinorSeventhChord("A#").name == "A#m7"
    assert chord.AugmentedChord("D#").name == "D#aug"


def test_major_chord():
    C = chord.MajorChord("C")
    assert C.scale == ChromaticScale("C")
    assert C.notes == [Note("C"), Note("E"), Note("G")]


def test_minor_chord():
    Cm = chord.MinorChord("Cm")
    assert Cm.notes == [Note("C"), Note("D#"), Note("G")]


def test_major_seventh():
    Cmaj7 = chord.MajorSeventhChord("C")
    assert Cmaj7.name == "Cmaj7"
    assert Cmaj7.notes == [Note("C"), Note("E"), Note("G"), Note("B")]


def test_minor_seventh():
    Cm7 = chord.MinorSeventhChord("C")
    assert Cm7.name == "Cm7"
    assert Cm7.notes == [Note("C"), Note("D#"), Note("G"), Note("A#")]


def test_augmented():
    Caug = chord.AugmentedChord("C")
    assert Caug.name == "Caug"
    assert Caug.notes == [Note("C"), Note("E"), Note("G#")]


def test_diminished():
    Cdim = chord.DiminishedChord("C")
    assert Cdim.name == "Cdim"
    assert Cdim.notes == [Note("C"), Note("D#"), Note("F#")]


def test_suspended_2():
    Csus2 = chord.SuspendedSecondChord("C")
    assert Csus2.name == "Csus2"
    assert Csus2.notes == [Note("C"), Note("D"), Note("G")]


def test_suspended_4():
    Csus4 = chord.SuspendedFourthChord("C")
    assert Csus4.name == "Csus4"
    assert Csus4.notes == [Note("C"), Note("F"), Note("G")]


def test_dominant_seventh():
    C7 = chord.DominantSeventhChord("C")
    assert C7.name == "C7"
    assert C7.notes == [Note("C"), Note("E"), Note("G"), Note("A#")]


def test_power_chord():
    C = chord.PowerChord("C")
    assert C.name == "C"
    assert C.notes == [Note("C"), Note("G")]
