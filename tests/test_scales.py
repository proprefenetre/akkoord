from akkoord import scales
from akkoord.notes import CHROMATIC_SCALE, Note


def test_chromatic_scale():
    assert scales.ChromaticScale("C").notes == [Note(n) for n in CHROMATIC_SCALE]
    assert scales.ChromaticScale("D#").notes[0] == Note("D#")


def test_chromatic_scale_with_note():
    assert scales.ChromaticScale(Note("C")).notes == [Note(n) for n in CHROMATIC_SCALE]
    assert scales.ChromaticScale(Note("D#")).notes[0] == Note("D#")


def test_diatonic_scale():
    scale = scales.MajorScale()
    assert scale.notes == [
        Note("C"),
        Note("D"),
        Note("E"),
        Note("F"),
        Note("G"),
        Note("A"),
        Note("B"),
    ]
    assert not any(n.sharp or n.flat for n in scale.notes)


def test_diatonic_scale_with_note():
    scale = scales.MajorScale(Note("C"))
    assert scale.notes == [
        Note("C"),
        Note("D"),
        Note("E"),
        Note("F"),
        Note("G"),
        Note("A"),
        Note("B"),
    ]
    assert not any(n.sharp or n.flat for n in scale.notes)


def test_chromatic_scale_enharmonic():
    scale = scales.ChromaticScale("C#")
    assert scale.enharmonic().notes == scales.ChromaticScale("Db").notes
    assert scale.notes == scales.ChromaticScale("C#").notes


def test_diatonic_scale_enharmonic():
    scale = scales.MinorScale("D")
    assert Note("Bb") in scale.enharmonic().notes


def test_scale_index():
    assert scales.ChromaticScale("C").index("C") == 0
    assert scales.MajorScale("C").index("C") == 0


def test_scale_getitem():
    assert scales.ChromaticScale("C")[0] == Note("C")
    assert scales.MajorScale("C")[0] == Note("C")
