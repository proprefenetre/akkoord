from akkoord.notes import Note
from akkoord.tuning import String, Tuning


def test_note():
    e = Note("E")
    assert e.sharp is False
    assert e.flat is False
    assert e.note == "E"


def test_sharp():
    a = Note("A#")
    assert a.sharp
    assert a.flat is False
    assert a.note == "A#"


def test_flat():
    a = Note("Gb")
    assert a.sharp is False
    assert a.flat
    assert a.note == "Gb"


def test_note_eq():
    assert Note("E") == Note("E")


def test_note_neq():
    assert Note("E") != Note("A")


def test_tuning_e():
    t = Tuning("E")
    assert len(t.strings) == 1


def test_string():
    s = String("E")
    notes = [n.note for n in s.notes]
    assert notes == ["E", "F", "F#", "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#"]


def test_string_frets():
    s = String("E")
    assert s.frets("G") == [3]


def test_string_note():
    s = String("E")
    assert s.note(3) == Note("G")
