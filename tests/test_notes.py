from akkoord import notes


def test_initialization():
    assert notes.Note("a").note == "A"
    assert notes.Note("C").note == "C"


def test_sharp():
    assert notes.Note("C#").sharp


def test_flat():
    assert notes.Note("Db").flat


def test_lower():
    C = notes.Note("C")
    assert C.lower() == notes.Note("B")
    assert C.lower(2) == notes.Note("A#")


def test_raise():
    C = notes.Note("C")
    assert C.raise_() == notes.Note("C#")
    assert C.raise_(2) == notes.Note("D")


def test_enharmonic():
    assert notes.Note("A#").enharmonic() == notes.Note("Bb")
    assert notes.Note("Ab").enharmonic() == notes.Note("G#")
